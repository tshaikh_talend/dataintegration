package routines;
import java.util.*;

/*
 * user specification: the function's comment should contain keys as follows: 1. write about the function's comment.but
 * it must be before the "{talendTypes}" key.
 * 
 * 2. {talendTypes} 's value must be talend Type, it is required . its value should be one of: String, char | Character,
 * long | Long, int | Integer, boolean | Boolean, byte | Byte, Date, double | Double, float | Float, Object, short |
 * Short
 * 
 * 3. {Category} define a category for the Function. it is required. its value is user-defined .
 * 
 * 4. {param} 's format is: {param} <type>[(<default value or closed list values>)] <name>[ : <comment>]
 * 
 * <type> 's value should be one of: string, int, list, double, object, boolean, long, char, date. <name>'s value is the
 * Function's parameter name. the {param} is optional. so if you the Function without the parameters. the {param} don't
 * added. you can have many parameters for the Function.
 * 
 * 5. {example} gives a example for the Function. it is optional.
 */
public class Vectors {

    /**
     * vector_difference: Returns the items in vector1 
     * that are not in vector2
     * 
     * 
     * {talendTypes} Object
     * 
     * {Category} Vectors
     * 
     * {param} Object(vector1) vector1: The first Vector
     * {param} Object(vector2) vector2: The second Vector
     * 
     * {example} vector_difference(vector1,vector2) # hello world !.
     */
    public static Vector<Integer> vector_difference(Object vector1, Object vector2)  {
    	Vector<Integer> diff_List = new Vector<Integer>();
    	
    	if (vector1 instanceof Vector<?> && vector2 instanceof Vector<?>)
    	{
    	
	    	for (Integer value: (Vector<Integer>)vector1)
	        {
	     	  if(!((Vector<Integer>)vector2).contains(value))
	     	  {
	     		  diff_List.add(value);
	     	  }
	        }

    	}
        return diff_List;
    	
    }
    
    /**
     * vector_compare: Returns true if the Vectors are identical, false if not
     * 
     * 
     * {talendTypes} Integer
     * 
     * {Category} Vectors
     * 
     * {param} Object(vector1) vector1: The first Vector
     * {param} Object(vector2) vector2: The second Vector
     * 
     * {example} vector_compare(vector1,vector2) # hello world !.
     */
    public static boolean vector_compare(Object vector1,Object vector2)  {
    	
    	if (!(vector1 instanceof Vector<?> && vector2 instanceof Vector<?>))
    	{
    		return false;
    	}
    	if(((Vector<Integer>)vector1).size() != ((Vector<Integer>)vector2).size())
 	   {
 		   return false;
 	   }
    	
    	for (int i=0;i<((Vector<Integer>)vector2).size();i++)
       {
    	   if(((Vector<Integer>)vector1).elementAt(i) != ((Vector<Integer>)vector2).elementAt(i))
    	   {
    	    	return false;
    	   }
    	  
       }
       return true;
    }
    
    /**
     * vector_insert: Inserts the items in vector2 into vector1
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} Vectors
     * 
     * {param} Object(vector1) vector1: The first Vector
     * {param} Object(vector2) vector2: The second Vector
     * {param} Integer(Position) vector2: The Position to insert
     * 
     * {example} vector_insert(vector1,vector2,Position) # hello world !.
     */
    public static Vector<Integer> vector_insert(Object vector1,Object vector2, Integer Position)  {
    	Vector<Integer> vectorInsert = new Vector<Integer>();
    	if (vector1 instanceof Vector<?> && vector2 instanceof Vector<?>)
    	{
    		for (Integer value:(Vector<Integer>)vector1)
        	{
        		vectorInsert.add(value);
        	}
        	for (int i = 0; i < ((Vector<Integer>)vector2).size(); i++)
            {
        		vectorInsert.add(Position + i,((Vector<Integer>)vector2).elementAt(i));
            }
    	}
    	
        return vectorInsert;
    }
    
    /**
     * vector_sorted_dedup_first: Deduplicates the Vector
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} Vectors
     * 
     * {param} Object vector1: The first Vector
     * 
     * {example} vector_sorted_dedup_first(vector1) # hello world !.
     */
    public static Vector<Integer> vector_sorted_dedup_first(Object vector)  {
    	Vector<Integer> vector_result = new Vector<Integer>();
    	if (vector instanceof Vector<?>)
    	{
	    	for (Integer value: (Vector<Integer>)vector)
	        {
	     	  if(!vector_result.contains(value))
	     	  {
	     		  vector_result.add(value);
	     	  }
	        }
    	}
    	return vector_result;
    }
    
    /**
     * StringtoVectorInt: Convert String list to Vector<Integer>
     * 
     * 
     * {talendTypes} Vector<Integer>
     * 
     * {Category} Vectors
     * 
     * {param} String(vector) vector1: The Vector
     * {param} String(",") split: The Separator
     * 
     * {example} StringtoVectorInt("1,2,3,4,5,6,7",",") # hello world !.
     */
    public static Vector<Integer> StringtoVectorInt(String vector,String split)  {
    	Vector<Integer> vector_result = new Vector<Integer>();
    	for (String value:vector.split("\\" + split))
        {
     	  vector_result.add(Integer.parseInt(value));
        }
    	return vector_result;
    }
    
    /**
     * VectorInttoString: Convert Vector<Integer> to String list 
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} Vectors
     * 
     * {param} Object(vector) vector: The Vector
     * {param} String(",") split: The Separator
     * 
     * {example} VectorInttoString(vector,",") # hello world !.
     */
    public static String VectorInttoString(Object vector,String split)  {
    	String list = "";
    	if (vector instanceof Vector<?>)
    	{
	    	for (Integer value:(Vector<Integer>)vector)
	        {
	     	  list += value.toString();
	     	  list += split;
	        }
	    	if (list.equals(""))
	    	{
	    		return "";
	    	}
	    	else
	    	{
	    		return list.substring(0,list.length()-1);
	    	}
    	}
    	else
    	{
    		return list;
    	}
    }
    
}
